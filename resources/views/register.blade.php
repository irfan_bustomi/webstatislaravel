<!DOCTYPE html>
<html>
    <head>
        <title>Tugas Harian</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="refresh" content="30">
    </head>
    <body>
        <h1>Buat Account Baru!</h1>
        <h3>Sign Up Form</h3>
        <form method="get" action="/welcome">
            
            <!--Contoh form text-->
            <label for="first_name">First Name :</label><br>
            <input type="text" id="first_name" name="first_name"> <br><br>

            <label for="last_name">Last Name :</label><br>
            <input type="text" id="last_name" name="last_name"> <br><br>

            <!--Contoh form radio-->
            <label for="gender">Gender :</label><br>
            <input type="radio" id="gender" name="gender" value="Male">Male<br>
            <input type="radio" id="gender" name="gender" value="Female">Female<br>
            <input type="radio" id="gender" name="gender" value="Other">Other<br><br>

            <!--Contoh form select option-->
            <label for="nationality">Nationality :</label><br>
            <select id="nationality" name="nationality">
                <option value="Indonesia">Indonesia</option>
                <option value="Singaporean">Singaporean</option>
                <option value="Malaysian">Malaysian</option>
                <option value="Australian">Australian</option>
            </select><br><br>

            <!--Contoh form checkbox-->
            <label for="language">Language Spoken :</label><br>
            <input type="checkbox" id="language" name="language" value="Bahasa Indonesia">Bahasa Indonesia<br>
            <input type="checkbox" id="language" name="language" value="English">English<br>
            <input type="checkbox" id="language" name="language" value="Other">Other<br><br>

            <!--Contoh form textarea-->
            <label for="bio">Bio :</label><br>
            <textarea id="bio" name="bio" cols="30" rows="10"></textarea><br><br>

            <input type="submit" value="Sign Up">
        </form>
    </body>
</html>