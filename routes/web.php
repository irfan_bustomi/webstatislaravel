<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
Route::get('home', 'pegawaiController@index');

Route::get('/hello/{angka}', function ($angka) {

    return view('hello',["angka" => $angka]);
});

Route::get('/hello-laravel', function () {
    echo "Ini adalah halaman baru <br>";
    return "Hello Laravel";
});

Route::get('/hello/{nama}', function($nama){
    return $nama;
});

Route::get('/form', 'RegisterController@form');
Route::get('/sapa', 'RegisterController@sapa');

Route::get('/', function () {
    return view('welcome');
});
*/

Route::get('/home', 'RegisterController@home');
Route::get('/register', 'RegisterController@register');
Route::get('/welcome', 'RegisterController@welcome');




